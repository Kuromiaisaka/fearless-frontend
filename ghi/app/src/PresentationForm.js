import React from 'react';

class PresentationForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      presenter_name: '',
      prsenter_email: '',
      company_name: '',
      title: '',
      synopsis: '',
      conference: '',
      conferences: [],
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangePresenterName = this.handleChangePresenterName.bind(this);
    this.handleChangePresenterEmail = this.handleChangePresenterEmail.bind(this);
    this.handleChangeCompanyName = this.handleChangeCompanyName.bind(this);
    this.handleChangeTitle = this.handleChangeTitle.bind(this);
    this.handleChangeSynopsis = this.handleChangeSynopsis.bind(this);
    this.handleChangeConference = this.handleChangeConference.bind(this);

  }

  async componentDidMount() {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ conferences: data.conferences });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.conferences
    const presentationUrl = 'http://localhost:8001/api/conferences/id/presentations';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const presentationResponse = await fetch(presentationUrl, fetchOptions);
    if (presentationResponse.ok) {
      this.setState({
        presenter_name: '',
        presenter_email: '',
        company_name: '',
        title: '',
        synopsis: '',
      });
    }
  }

  handleChangePresenterName(event) {
    const value = event.target.value;
    this.setState({ presenterName: value });
  }

  handleChangePresenterEmail(event) {
    const value = event.target.value;
    this.setState({ presenterEmail: value });
  }

  handleChangeCompanyName(event) {
    const value = event.target.value;
    this.setState({ companyName: value });
  }
  handleChangeTitle(event){
    const value = event.target.value;
    this.setState({ title: value });
  }
  handleChangeSynopsis(event){
    const value = event.target.value;
    this.setState({ synopsis: value });
  }
  handleChangeConference(event){
    const value = event.target.value;
    this.setState({ conference: value});
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Choose a presentation</h1>
            <form onSubmit={this.handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChangePresenterName} placeholder="presenterName" required type="text" name="presenterName" id="presenterName" className="form-control" />
                <label htmlFor="presenterName">Presenter Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangePresenterEmail} placeholder="presenterEmail" required type="text" name="pretenderEmail" id="presenterEmail" className="form-control" />
                <label htmlFor="presenterEmail">Presenter Email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeCompanyName} placeholder="companyName" required type="text" name="companyName" id="companyName" className="form-control" />
                <label htmlFor="companyName">Company Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeTitle} placeholder="title" required type="text" name="title" id="title" className="form-control" />
                <label htmlFor="title">Title</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeSynopsis} placeholder="synopsis" required type="text" name="synopsis" id="synopsis" className="form-control" />
                <label htmlFor="synopsis">Synopsis</label>
              </div>
              <div className="mb-3">
              <select onChange={this.handleChangeConference} required id="conference" name="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {this.state.conferences ? this.state.conferences.map(conference => {
                    return (
                      <option key={conference.id} value={conference.id}>
                        {conference.name}
                      </option>
                    );
                  }):null}
              </select> 
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default PresentationForm;