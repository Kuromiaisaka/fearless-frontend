import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import React from 'react';
import ConferenceForm from './ConferenceForm'
import AttendConferenceForm from './AttendConferenceForm'
import PresentationForm from './PresentationForm'
import { BrowserRouter, Route, Routes } from "react-router-dom";
import MainPage from './MainPage'

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
          <Routes>
            <Route path="conferences/new" element={<ConferenceForm />} />
            <Route path="attendees/new" element={<AttendConferenceForm />} />
            <Route path="locations/new" element={<LocationForm />} />
            <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
            <Route path="presentations/new" element={<PresentationForm />} /> 
            <Route index element={<MainPage />} />
          </Routes>
    </BrowserRouter>
  );
}

export default App;
